# x86 Assembly Examples

Some notes and example programs written in x86 assembly for 64-bit Linux.

## ToC
- [Immediate Operands](#Immediate-Operands)
- [Memory Operands](#Memory-Operands)
- [Defining Data and Reserving Space](#Defining-Data-and-Reserving-Space)
- [General Purpose Registers (GPRs)](#General-Purpose-Registers-GPRs)
  - [Integer Registers](#Integer-Registers)
  - [Vector Registers](#Vector-Registers)

## Immediate Operands

```asm
200          ; decimal
0200         ; still decimal - the leading 0 does not make it octal
0200d        ; explicitly decimal - d suffix
0d200        ; also decimal - 0d prefex
0c8h         ; hex - h suffix, but leading 0 is required because c8h looks like a var
0xc8         ; hex - the classic 0x prefix
0hc8         ; hex - for some reason NASM likes 0h
310q         ; octal - q suffix
0q310        ; octal - 0q prefix
11001000b    ; binary - b suffix
0b1100_1000  ; binary - 0b prefix, and by the way, underscores are allowed
```

## Memory Operands

Valid forms of addressing are:

- `[number]`
- `[register]`
- `[register + register*scale]` *(`scale` MUST be either 1, 2, 4 or 8)*
- `[register + register*scale + number]`

The `number` is called a *displacement*, `register` is called *base* and the
`register` with the `scale` is called the *index*.

Examples:
```asm
[750]                  ; displacement only
[rbp]                  ; base register only
[rcx + rsi*4]          ; base + index * scale
[rbp + rdx]            ; scale is 1
[rbx - 8]              ; displacement is -8
[rax + rdi*8 + 500]    ; all four components
[rbx + counter]        ; uses the address of the variable 'counter' as the displacement
```

## Defining Data and Reserving Space

```asm
db    0x55                ; just the byte 0x55
db    0x55,0x56,0x57      ; three bytes in succession
db    'a',0x55            ; character constants are OK
db    'hello',13,10,'$'   ; so are string constants
dw    0x1234              ; 0x34 0x12
dw    'a'                 ; 0x61 0x00 (it's just a number)
dw    'ab'                ; 0x61 0x62 (character constant)
dw    'abc'               ; 0x61 0x62 0x63 0x00 (string)
dd    0x12345678          ; 0x78 0x56 0x34 0x12
dd    1.234567e20         ; floating-point constant
dq    0x123456789abcdef0  ; eight byte constant
dq    1.234567e20         ; double-precision float
dt    1.234567e20         ; extended-precision float
```

Reserve *uninitialized* space:

```asm
buffer:         resb    64              ; reserve 64 bytes
wordvar:        resw    1               ; reserve a word
realarray:      resq    10              ; array of ten reals
```

## General Purpose Registers (GPRs)

### Integer Registers
64-bit | 32-bit | 16-bit | 8-bit high | 8-bit low
------ | ------ | ------ | ---------- | ---------
rax    | eax    | ax     | ah         | al
rcx    | ecx    | cx     | ch         | cl
rdx    | edx    | dx     | dh         | dl
rbx    | ebx    | bx     | bh         | bl
rsp    | esp    | sp     |            | spl
rbp    | ebp    | bp     |            | bpl
rsi    | esi    | si     |            | sil
rdi    | edi    | di     |            | dil
r8     | r8d    | r8w    |            | r8b
r9     | r9d    | r9w    |            | r9b
r10    | r10d   | r10w   |            | r10b
r11    | r11d   | r11w   |            | r11b
r12    | r12d   | r12w   |            | r12b
r13    | r13d   | r13w   |            | r13b
r14    | r14d   | r14w   |            | r14b
r15    | r15d   | r15w   |            | r15b

> Notes:
> - Note: Operations that output to a 32-bit subregister are automatically
>   zero-extended to the entire 64-bit register. Operations that output to 8-bit
>   or 16-bit subregisters are *not* zero-extended.
> - ah, bh, ch and dh can't be used with all types of operands, they still
>   exist mainly for backward-compatibility.

### Vector Registers

| Instruction Set   | Registers                             | Width (Bits)  |
| ----------------- | ------------------------------------- | ------------- |
| SSE               | xmm0..xmm7                            | 128           |
| SSE2, SSE3, SSSE3 | xmm0..xmm15                           | 128           |
| SSE4              | xmm0..xmm31                           | 128           |
| AVX               | xmm0..xmm31, ymm0..ymm15              | 128, 256      |
| AVX2              | xmm0..xmm31, ymm0..ymm31              | 128, 256      |
| AVX-512           | xmm0..xmm31, ymm0..ymm31, zmm0..zmm31 | 128, 256, 512 |

