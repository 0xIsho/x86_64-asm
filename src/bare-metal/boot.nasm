;-------------------------------------------------------------------------------
; The most bare-bones MBR boot code in existence.
;
; It does absolutely nothing other than pretending to be a valid MBR boot sector
;
; Assemble:
;   nasm -f bin -o boot.bin boot.nasm
; Execute:
;   qemu-system-x86_64 boot.bin
;-------------------------------------------------------------------------------

cli                 ; Disable interrupts
.loop:
	hlt             ; Halt the CPU
	jmp .loop       ; Jump back to .loop

times 510 - ($-$$) db 0 ; Zero pad the rest of the binary
dw 0xAA55               ; Boot signature
