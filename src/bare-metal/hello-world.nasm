;-------------------------------------------------------------------------------
; Hello World example running on bare metal
;
; Assemble:
;   nasm -f bin -o hello-world.bin hello-world.nasm
; Execute:
;   qemu-system-x86_64 hello-world.bin
;-------------------------------------------------------------------------------

mov ax, 0x0003                ; Set video mode to 80x25 text mode
int 0x10                      ; Call BIOS interrupt 10h

; Set the segment register to 0xB800, which is the address of VGA memory
mov ax, 0xB800
mov es, ax

; The values 0x0FYY0FXX mean the following:
; - 0F is the attribute byte, <background><foreground>. Here it means White on
;   Black.
; - XX is the ASCII value of the first character to be printed
; - YY is the ASCII value of the second character to be printed
; - The values are laid out right-to-left, 0FXX being the first character and
;   0FYY being the second, because x86 is a little-endian architecture.
mov [es:0 ], dword 0x0F650F48 ; "He"
mov [es:4 ], dword 0x0F6C0F6C ; "ll"
mov [es:8 ], dword 0x0F2C0F6F ; "o,"
mov [es:12], dword 0x0F570F20 ; " W"
mov [es:16], dword 0x0F720F6F ; "or"
mov [es:20], dword 0x0F640F6C ; "ld"
mov [es:24], dword 0x0F210F21 ; "!!"

halt:
	cli                       ; Disable interrupts
	.loop:
		hlt                   ; Halt the CPU
		jmp .loop             ; Jump back to .loop

times 510 - ($-$$) db 0       ; Zero pad the rest of the binary
dw 0xAA55                     ; Boot signature
