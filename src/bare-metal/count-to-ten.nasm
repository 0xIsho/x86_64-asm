;-------------------------------------------------------------------------------
; Counts to ten. Well, not really. It actually counts 0..9, simply because
; it's much more simple :D
;
; Assemble:
;   nasm -f bin -o count-to-ten.bin count-to-ten.nasm
; Execute:
;   qemu-system-x86_64 count-to-ten.bin
;-------------------------------------------------------------------------------

mov ax, 0x0003          ; Set video mode to 80x25 text mode
int 0x10                ; Call BIOS interrupt 10h

mov ax, 0xB800          ; Set the segment register to the address..of VGA
mov es, ax              ; ..memory. That's where we'll be printing stuff to

mov cx, 0               ; Loop counter
mov bh, 0x0F            ; Set the character attributes (foreground+background)..
                        ; ..Black on White
loop:
	mov di, cx          ; Offset into VGA memory (MUST be in `di`)
	shl di, 1           ; Multiply by 2 to get byte position
	mov bl, '0'         ; Load ASCII code for '0' then..
	add bl, cl          ; .."Shift" the code by the counter to get the character code
	mov [es:di], bx     ; Copy to VGA memory at offset `di` (computed earlier)
	inc cx              ; Increment counter
	cmp cx, 10          ; Compare against 10
	jl loop             ; Jump if less

halt:
	cli                 ; Disable interrupts
	.loop:
		hlt             ; Halt the CPU
		jmp .loop       ; Jump back to .loop

times 510 - ($-$$) db 0 ; Zero pad the rest of the binary
dw 0xAA55               ; Boot signature
