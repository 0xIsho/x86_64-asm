;-------------------------------------------------------------------------------
; Writes "Hello, World!" to stdio. Runs on 64-bit Linux.
;
; Assemble using:
; 	nasm -f elf64 hello.nasm -o hello.o && ld hello.o
; Execute:
; 	./a.out
;-------------------------------------------------------------------------------

	global _start

	section .text
_start:
	mov rax, 1                ; System call for `write`
	mov rdi, 1                ; File handle (1 = stdout)
	mov rsi, message          ; Address of string to be written
	mov rdx, 15               ; Number of bytes to output
	syscall                   ; Invoke the system call

	mov rax, 60               ; System call for exit
	xor rdi, rdi              ; Exit status (0)
	syscall                   ; Invoke system call

	section .data
message:
	db "Hello, World!", 10, 0 ; 10 is a new-line character
